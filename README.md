#正则表达式"30分钟"入门教程#

###正品地址: [http://deerchao.net/tutorials/regex/regex.htm](http://deerchao.net/tutorials/regex/regex.htm)###
###页面假装美化版地址: [http://www.sub-273.com/demo/RegEx/regex.html](http://www.sub-273.com/demo/RegEx/regex.html)###


这个教程是[deerchao](http://deerchao.net/)写的, 老夫只是把页面效果简单的改了改, 仿佛能稍微好看那么一点.

**强烈建议去原作者那看**, 老夫这里的代码更新.. 取决于啥时候意识到作者更新(- -).

附上一张高清五码的"美化图". 求别吐槽..

![美化后的正则表达式入门教程截图](https://bitbucket.org/theReturnOfTheMonk/learn-regexp/raw/902e56c62df79a60d8f0feeebc83d75d524a8a27/images/screen_shot_of_regExp.png)

#TODO#
 - 有心搞一个Markdown版的. 看时间吧.
